package com.example.util.exception;

public class ValorInvalidoException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public ValorInvalidoException() { }
	
	public ValorInvalidoException (String msg) {
		super(msg);
	}
}
