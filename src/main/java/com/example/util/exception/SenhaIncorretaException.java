package com.example.util.exception;

public class SenhaIncorretaException extends Exception {
	private static final long serialVersionUID = 1L;

	public SenhaIncorretaException() { }
	
	public SenhaIncorretaException (String msg) {
		super(msg);
	}
}
