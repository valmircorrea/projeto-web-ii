package com.example.util.exception;

public class SenhaInvalidaException extends Exception {
	private static final long serialVersionUID = 1L;

	public SenhaInvalidaException() { }
	
	public SenhaInvalidaException (String msg) {
		super(msg);
	}
}
