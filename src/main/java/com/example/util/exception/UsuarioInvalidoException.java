package com.example.util.exception;

public class UsuarioInvalidoException extends Exception {
	private static final long serialVersionUID = 1L;

	public UsuarioInvalidoException() { }
	
	public UsuarioInvalidoException (String msg) {
		super(msg);
	}
}
