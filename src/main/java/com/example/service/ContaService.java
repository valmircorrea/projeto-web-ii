package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.model.Conta;
import com.example.repository.ContaRepository;
import com.example.util.exception.SenhaIncorretaException;
import com.example.util.exception.SenhaInvalidaException;
import com.example.util.exception.UsuarioInvalidoException;

import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class ContaService {

	@Autowired
	private ContaRepository contaRepository;

	public List<Conta> findAll() {
		return contaRepository.findAll();
	}

	public Optional<Conta> findOne(Integer id) {
		return contaRepository.findById(id);
	}
	
	@Transactional(readOnly = false)
	public Conta save(Conta entity) throws UsuarioInvalidoException, SenhaInvalidaException, SenhaIncorretaException {
		this.verificaTamSenha(entity);
		this.verificaUser(entity);
		
		return contaRepository.save(entity);
	}

	@Transactional(readOnly = false)
	public void delete(Conta entity) {
		contaRepository.delete(entity);
	}	
	
	/**
	 * Verifica se a senha é menos que 8 digitos
	 * @param id
	 * @return
	 * @throws SenhaInvalidaException 
	 */
	public void verificaTamSenha(Conta entity) throws SenhaInvalidaException {
		if (entity.getPassword().length() < 8) {
			throw new SenhaInvalidaException();
		}
	}
	
	/**
	 * Verifica se o id ja esta no banco
	 * @param entity
	 * @return true or false
	 * @throws UsuarioInvalidoException 
	 * @throws SenhaIncorretaException 
	 */
	public void verificaUser(Conta entity) throws UsuarioInvalidoException, SenhaIncorretaException {
		List<Conta> entitys = contaRepository.findAll();
		
		for(Conta conta : entitys) {
			if (conta.getCode().equals(entity.getCode())) {
				if (conta.getId() != entity.getId()) {
					throw new UsuarioInvalidoException();
				}
			}
			
			if (!(conta.getPassword().equals(entity.getPassword()))) {
				throw new SenhaIncorretaException();
			}
		}
	}

}
	
