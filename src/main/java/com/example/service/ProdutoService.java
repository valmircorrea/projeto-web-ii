package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Conta;
import com.example.model.Produto;
import com.example.repository.ProdutoRepository;
import com.example.util.exception.ValorInvalidoException;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;

	public List<Produto> findAll() {
		return produtoRepository.findAll();
	}
	
	public Optional<Produto> findOne(Integer id) {
		return produtoRepository.findById(id);
	}
	
	@Transactional(readOnly = false)
	public Produto save(Produto entity) throws ValorInvalidoException {
		this.verificaValor(entity);
		
		return produtoRepository.save(entity);
	}

	@Transactional(readOnly = false)
	public void delete(Produto entity) {
		produtoRepository.delete(entity);
	}
	
	@Transactional(readOnly = false)
	public Produto vendido (Produto entity) {
		if (entity.isVendido()) {
			entity.setVendido(false);
		} else {
			entity.setVendido(true);
		}
		
		return produtoRepository.save(entity);
	}
	
	public void verificaValor (Produto entity) throws ValorInvalidoException {
		if (entity.getValor() <= 0) {
			throw new ValorInvalidoException();
		}
	}

}
	
