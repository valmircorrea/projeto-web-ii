package com.example.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.model.Conta;
import com.example.model.Produto;
import com.example.service.ContaService;
import com.example.service.ProdutoService;
import com.example.util.exception.ValorInvalidoException;

@Controller
@RequestMapping("/produtos")
public class ProdutoController {
	
	private static final String MSG_SUCESS_INSERT = "Produto inserido com sucesso.";
	private static final String MSG_SUCESS_UPDATE = "Produto modificado com sucesso.";
	private static final String MSG_SUCESS_DELETE = "Produto deletado com sucesso.";
	private static final String MSG_SUCESS_VENDER = "Produto marcado como vendido com sucesso.";
	private static final String MSG_VALOR_INVALIDO = "O valor do produto deve ser maior que 0";
	private static final String MSG_ERROR = "Erro na inserção do Produto";
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private ContaService contaService; //module service

	// Primeira tela da pagina de produtos
	@GetMapping
	public String index(Model model) {
		List<Produto> all = produtoService.findAll();
		model.addAttribute("listProduto", all);
		model.addAttribute("");
		return "produto/index";
	}
	
	// Tela de Show produto
	@GetMapping("/{id}")
	public String show(Model model, @PathVariable("id") Integer id) {
		if (id != null) {
			Produto produto = produtoService.findOne(id).get();
			model.addAttribute("produto", produto);
		}
		return "produto/show";
	}

	// Tela com Formulario de New produto
	@GetMapping(value = "/new")
	public String create(Model model, @ModelAttribute Produto entityProduto, 
			             @ModelAttribute Conta entityModule) {
		List<Conta> all = contaService.findAll();
		model.addAttribute("modules", all);
		
		return "produto/form";
	}
	
	// Processamento do formulario New produto (ou Alter produto) 
	@PostMapping
	public String create(@Valid @ModelAttribute Produto entityProduto, 
			             @Valid @ModelAttribute Conta entityConta,
			             BindingResult result, RedirectAttributes redirectAttributes) {
		Produto produto = null;
		 
		try {
			produto = produtoService.save(entityProduto);
			redirectAttributes.addFlashAttribute("success", MSG_SUCESS_INSERT);
			return "redirect:/conta";
		} catch (ValorInvalidoException ve) {
			ve.printStackTrace();
			redirectAttributes.addFlashAttribute("error", MSG_VALOR_INVALIDO);
		} catch (Exception e) {
			e.printStackTrace();
			redirectAttributes.addFlashAttribute("error", MSG_ERROR);
		}catch (Throwable e) {
			e.printStackTrace();
			redirectAttributes.addFlashAttribute("error", MSG_ERROR);
		}
		
		return "redirect:/produtos/new";
	}
	
	@GetMapping("/{id}/edit")
	public String update(Model model, @PathVariable("id") Integer id) {
		
		try {
			if (id != null) {
				List<Conta> all = contaService.findAll();
				model.addAttribute("modules", all);
				
				Produto entity = produtoService.findOne(id).get();
				model.addAttribute("produto", entity);
			}
		} catch (Exception e) {
			throw new ServiceException(e.getMessage());
		}
		return "produto/form";
	}
	
	@PutMapping
	public String update(@Valid @ModelAttribute Produto entity, BindingResult result, 
			             RedirectAttributes redirectAttributes) {
	
		String retorno = null;
		Produto produto = null;
		try {
			produto = produtoService.save(entity);
			redirectAttributes.addFlashAttribute("success", MSG_SUCESS_UPDATE);
			retorno = "redirect:/produtos/" + produto.getId();
		} catch (ValorInvalidoException ve) {
			ve.printStackTrace();
			redirectAttributes.addFlashAttribute("error", MSG_VALOR_INVALIDO);
			retorno = "redirect:/produtos/" + entity.getId() + "/edit";
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", MSG_ERROR);
			e.printStackTrace();
			retorno = "redirect:/produtos/" + entity.getId() + "/edit";
		}
		return retorno;
	}
	
	@RequestMapping("/{id}/delete")
	public String delete(@PathVariable("id") Integer id, RedirectAttributes redirectAttributes) {
		try {
			if (id != null) {
				Produto entity = produtoService.findOne(id).get();
				produtoService.delete(entity);
				redirectAttributes.addFlashAttribute("success", MSG_SUCESS_DELETE);
			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", MSG_ERROR);
			throw new ServiceException(e.getMessage());
		}
		return "redirect:/produtos/";
	}
	
	@RequestMapping("/{id}/vender")
	public String vender(@PathVariable("id") Integer id, RedirectAttributes redirectAttributes) {
		try {
			if (id != null) {
				Produto entity = produtoService.findOne(id).get();
				produtoService.vendido(entity);
				redirectAttributes.addFlashAttribute("success", MSG_SUCESS_VENDER);
			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", MSG_ERROR);
			throw new ServiceException(e.getMessage());
		}
		return "redirect:/conta/";
	}
	
	@RequestMapping("/search")
	public String search(Model model,@RequestParam("nome") String busca, @RequestParam(value = "ord", required = false) String ord) {
		
		List<Produto> all = produtoService.findAll();
		List<Produto> search = new ArrayList<Produto>();
		
		model.addAttribute("busca", busca);
		
		Produto produtoBuscado = new Produto();
		produtoBuscado.setNome(busca);
		
		model.addAttribute("produtoBuscado", produtoBuscado);
		
		for (Produto produto : all) {
			if ((produto.getNome().toLowerCase().contains(busca.toLowerCase())) && (!produto.isVendido())) {
				search.add(produto);
			}
		}
		
		if (ord != null) {
			if (ord.equals("2")) {
				Collections.sort(search, Collections.reverseOrder());
			} else if (ord.equals("1")) {
				Collections.sort(search);
			} else if (ord.equals("3")) {
				Collections.reverse(search);
			}
		}
		
		System.out.println("***************** " + busca);
		
		model.addAttribute("produtos", search);
		
		return "produto/search";
	}
}
