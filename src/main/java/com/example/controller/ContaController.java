package com.example.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.model.Conta;
import com.example.model.Produto;
import com.example.service.ContaService;
import com.example.util.exception.SenhaIncorretaException;
import com.example.util.exception.SenhaInvalidaException;
import com.example.util.exception.UsuarioInvalidoException;

@Controller
@RequestMapping("/conta")
public class ContaController {

	private static final String MSG_SUCESS_INSERT = "Cadastro realizado com sucesso!";
	private static final String MSG_SUCESS_UPDATE = "Informações atualizadas com sucesso!";
	private static final String MSG_SUCESS_DELETE = "Conta deletada com sucesso!";
	private static final String MSG_USER_EXISTS = "Usuário já cadastrado!";
	private static final String MSG_LOGIN_ERROR = "Usuário e/ou senha incorretos!";
	private static final String MSG_SENHA_INVALIDA = "Sua senha deve conter no minimo 8 caracteres!";
	private static final String MSG_SENHA_INCORRETA =  "Senha incorreta!";
	private static final String MSG_ERROR = "Error.";

	@Autowired
	private ContaService contaService;
	

	@GetMapping
	public String index(Model model) {
		List<Conta> all = contaService.findAll();
		model.addAttribute("listconta", all);
		return "conta/index";
	}
	
	@GetMapping("/{id}")
	public String show(Model model, @PathVariable("id") Integer id) {
		if (id != null) {
			Conta conta = contaService.findOne(id).get();
			model.addAttribute("conta", conta);
		}
		return "conta/show";
	}

	@GetMapping(value = "/new")
	public String create(Model model, @ModelAttribute Conta entityconta, @ModelAttribute Produto entityStudent) {
		model.addAttribute("conta", entityconta);
		
		return "conta/form";
	}
	
	@PostMapping
	public String create(@Valid @ModelAttribute Conta entity, BindingResult result, RedirectAttributes redirectAttributes) {
		
			String retorno = "redirect:/conta/new";
			try {
				contaService.save(entity);
				redirectAttributes.addFlashAttribute("success", MSG_SUCESS_INSERT);
				retorno = "redirect:/conta";
			} catch (SenhaInvalidaException e) {
				e.printStackTrace();
				redirectAttributes.addFlashAttribute("error", MSG_SENHA_INVALIDA);
			} catch (UsuarioInvalidoException u) {
				u.printStackTrace();
				redirectAttributes.addFlashAttribute("error", MSG_USER_EXISTS);
			} catch (Exception e) {
				e.printStackTrace();
				redirectAttributes.addFlashAttribute("error", MSG_ERROR);
			}
			return retorno;
	}
	
	@GetMapping("/{id}/edit")
	public String update(Model model, @PathVariable("id") Integer id) {
		try {
			if (id != null) {
				Conta entity = contaService.findOne(id).get();
				model.addAttribute("conta", entity);
			}
		} catch (Exception e) {
			throw new ServiceException(e.getMessage());
		}
		return "conta/form";
	}
	
	@PutMapping
	public String update(@Valid @ModelAttribute Conta entity, BindingResult result, RedirectAttributes redirectAttributes) {
		String retorno = "redirect:/conta/" + entity.getId() + "/edit";
		try {
			contaService.save(entity);
			redirectAttributes.addFlashAttribute("success", MSG_SUCESS_UPDATE);
			retorno = "redirect:/conta/" + entity.getId();
		} catch (SenhaInvalidaException e) {
			e.printStackTrace();
			redirectAttributes.addFlashAttribute("error", MSG_SENHA_INVALIDA);
		} catch (UsuarioInvalidoException u) {
			u.printStackTrace();
			redirectAttributes.addFlashAttribute("error", MSG_USER_EXISTS);
		} catch (SenhaIncorretaException e) {
			redirectAttributes.addFlashAttribute("error", MSG_SENHA_INCORRETA);
			e.printStackTrace();
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", MSG_ERROR);
			e.printStackTrace();
		}
		
		return retorno;
	}
	
	@RequestMapping("/{id}/delete")
	public String delete(@PathVariable("id") Integer id, RedirectAttributes redirectAttributes) {
		try {
			if (id != null) {
				Conta entity = contaService.findOne(id).get();
				contaService.delete(entity);
				redirectAttributes.addFlashAttribute("success", MSG_SUCESS_DELETE);
			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", MSG_ERROR);
			throw new ServiceException(e.getMessage());
		}
		return "redirect:/conta/";
	}
	
	/**
	 * controle do login
	 */
	  @RequestMapping("/loginProcess")
	  public String loginProcess(@ModelAttribute("login") Conta entityLogin, RedirectAttributes redirectAttributes) {
		String retorno = null;
		  try {
			  contaService.verificaTamSenha(entityLogin);
			  Integer id = entityLogin.getId();
			  retorno = "redirect:/conta/" + id;
			} catch (SenhaInvalidaException e) {
				e.printStackTrace();
				redirectAttributes.addFlashAttribute("error", MSG_LOGIN_ERROR);
		    	retorno = "redirect:/login";
			} 
	    return retorno;
	}
}
