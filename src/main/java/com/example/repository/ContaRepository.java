package com.example.repository;
import org.springframework.stereotype.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import com.example.model.Conta;

@Repository
public interface ContaRepository extends JpaRepository<Conta, Integer> {
	
}